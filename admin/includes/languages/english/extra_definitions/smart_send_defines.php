<?php
//
// +----------------------------------------------------------------------+
// |zen-cart Open Source E-commerce                                       |
// +----------------------------------------------------------------------+
// | Copyright (c) 2004 The zen-cart developers                           |
// |                                                                      |
// | http://www.zen-cart.com/index.php                                    |
// |                                                                      |
// +----------------------------------------------------------------------+

//  $Id: csmart_send_defines.php $
//
// FROM SMART SEND MODULE
// Added dimensions to products
//
define('TEXT_PRODUCTS_LENGTH', 'Length');
define('TEXT_PRODUCTS_WIDTH', 'Width');
define('TEXT_PRODUCTS_HEIGHT', 'Height');