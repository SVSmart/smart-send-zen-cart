<?php

require_once( DIR_FS_CATALOG . DIR_WS_FUNCTIONS . 'smartsend_functions.php' );

class smartsend extends base
{
	var $code, $title, $description, $icon, $sort_order, $tax_class, $enabled;

	function smartsend()
	{
		global $order, $db, $template;

		$this->code = 'smartsend';
		$this->title = MODULE_SHIPPING_SMARTSEND_TEXT_TITLE;
		$this->description = MODULE_SHIPPING_SMARTSEND_TEXT_DESCRIPTION;
		$this->icon = DIR_WS_IMAGES . '/smart_send_logo.jpg';
		$this->sort_order = MODULE_SHIPPING_SMARTSEND_SORT_ORDER;
		$this->tax_class = MODULE_SHIPPING_SMARTSEND_TAX_CLASS;
		$this->enabled = (MODULE_SHIPPING_SMARTSEND_STATUS == 'True') ? true : false;

		// Configuration options
		$this->_setConfigKeys();
	}

	/**
	* Get quote from shipping provider's API:
	*
	* @param string $method
	* @return array of quotation results
	*/

	function quote( $method = '' )
	{
		global $db, $order, $cart;

		$orderInfo = $order->info;
		$orderDelivery = $order->delivery;

		if( $orderDelivery['country']['iso_code_2'] != 'AU' ) return; // Must be to Australia

		$fromPostcode = MODULE_SHIPPING_SMARTSEND_FROM_POSTCODE;
		// If no suburb, use city
		$fromTown = MODULE_SHIPPING_SMARTSEND_FROM_TOWN;
		$fromState = smartSendUtils::getState( $fromPostcode );

		$toPostcode = $orderDelivery['postcode'];
		$toTown = ( !empty( $orderDelivery['suburb'] ) ) ? $orderDelivery['suburb'] : $orderDelivery['city'];
		$toState = smartSendUtils::getState( $toPostcode );

		$errors = array();
		$itemList = array();
		$pickupFlag = $deliveryFlag = false;

		$cartItems = $_SESSION['cart']->get_products();

		// Loop through the cart items
		foreach( $cartItems as $item )
		{
			$itemWeight = ceil( $item['weight'] );
			if( !$itemWeight ) $errors[] = 'Cart item '  . $item['name'] . ' has no weight set.';

			// Let's get our added dimensions
			$qry = "
			SELECT products_length, products_height, products_width
			FROM " . TABLE_PRODUCTS . "
			WHERE products_id='$item[id]'
			LIMIT 1 ";
			$res = $db->Execute( $qry );
			if( $res->RecordCount() > 0 )
			{
				$dims = $res->fields;
				$itemLength = ceil( ( $dims['products_length'] > 0 ) ? $dims['products_length'] : MODULE_SHIPPING_SMARTSEND_LENGTH );
				$itemWidth = ceil( ($dims['products_width'] > 0 ) ? $dims['products_width'] : MODULE_SHIPPING_SMARTSEND_WIDTH );
				$itemHeight = ceil( ($dims['products_height'] > 0 ) ? $dims['products_height'] : MODULE_SHIPPING_SMARTSEND_HEIGHT );
			}

			// Flags for tail-lift
			if( MODULE_SHIPPING_SMARTSEND_TAIL_LIFT_DELIVERY == 'Yes' && $itemWeight >= 30 )  $deliveryFlag = true;
			if( MODULE_SHIPPING_SMARTSEND_TAIL_LIFT > 0  && $itemWeight >= MODULE_SHIPPING_SMARTSEND_TAIL_LIFT ) $pickupFlag = true;

			foreach ( range( 1, $item['qty'] ) as $blah )
			{
				// Compile details
				$itemList[] = array(
					'Description'   => MODULE_SHIPPING_SMARTSEND_TYPE,
					'Weight'        => $itemWeight,
					'Depth'         => $itemWidth,
					'Length'        => $itemLength,
					'Height'        => $itemHeight
					);
			}
		}

		if( count( $errors ) )
		{
			$this->quotes['error'] = implode( "<br>\n", $errors );
			return $this->quotes;
		}

		$testServer = ( MODULE_SHIPPING_SMARTSEND_LIVE == 'Test' ) ? true : false;

		$smartSendQuote = new smartSendUtils( MODULE_SHIPPING_SMARTSEND_USERNAME, MODULE_SHIPPING_SMARTSEND_PASSWORD, $testServer );

		if( count( $itemList ) )
		{
			// Transport assurance
			$assuranceOpt = MODULE_SHIPPING_SMARTSEND_ASSURANCE;
			if( $assuranceOpt == 'Yes' && $cart->total > MODULE_SHIPPING_SMARTSEND_ASSURANCE_MIN )
			{
				// Set transport assurance option to the total of the cart
				// This is normally set to the wholesale value of the goods but that data is not available
				$smartSendQuote->setOptional( 'transportAssurance', $cart->total );
			}

			// Tail-lift options
			$tailLift = 'NONE'; // Default
			if( $pickupFlag ) $tailLift = 'PICKUP';
			if( $deliveryFlag )
			{
				if( $pickupFlag ) $tailLift = 'BOTH';
				else $tailLift = 'DELIVERY';
			}
			$smartSendQuote->setOptional( 'tailLift', $tailLift );

			// Handling Fee
			$feeType = MODULE_SHIPPING_SMARTSEND_FEE_TYPE;
			if( $feeType == 'Flat' )
				$handling = MODULE_SHIPPING_SMARTSEND_FEE_AMT;
			else if( $feeType == 'Percentage' )
				$handling = sprintf( "%1.2f", $cart->total * MODULE_SHIPPING_SMARTSEND_FEE_AMT / 100 );

			if( !$fromPostcode || !$fromTown )
			{
				$errors[] = "You must set the origin postcode and town in Smart Send settings.";
			}

			if( !$toPostcode )
				$errors[] = "No destination postcode given.";

			if( count( $errors ) )
			{
				$this->quotes['error'] = implode( "<br>\n", $errors );
				return $this->quotes;
			}
			
			$smartSendQuote->setFrom(
				array( $fromPostcode, $fromTown, $fromState )
				);

			if (!$toTown)
				$toTown = $smartSendQuote->getFirstTown($toPostcode);

			$smartSendQuote->setTo(
				array( $toPostcode, $toTown, $toState )
				);			

			foreach( $itemList as $item )
				$smartSendQuote->addItem( $item );

			$quoteResult = $smartSendQuote->getQuote();

			if( $quoteResult->ObtainQuoteResult->StatusCode != 0 )
			{
				$this->quotes['error'] = 'Shipping calculation error: ' . print_r( $quoteResult->ObtainQuoteResult->StatusMessages->string, true );
				return $this->quotes;
			}

			$quotes = $quoteResult->ObtainQuoteResult->Quotes->Quote;
			
			$useQuotes = array();

			if( !@is_array( $quotes ) ) $useQuotes[0] = $quotes;
			else $useQuotes = $quotes;

			$methods = array();

			$i = 0;
			foreach( $useQuotes as $quote )
			{
				if( empty($quote->TotalPrice) ) continue;
				$rateTotal = $quote->TotalPrice + $handling;
				$methodCode = smart_send_slugify( $this->code . ' ' . $quote->CourierName . ' ' . $quote->ServiceName );
				$methods[] = array(
					'id'			=> $methodCode,
					'title'		=> $quote->TransitDescription,
					'cost'		=> $rateTotal
					);
			}
		}
		
		$this->quotes = array(
			'id' => $this->code,
			'module' => 'Courier',
			'methods' => $methods
		);
		smart_send_debug_log( 'quotes', $this->quotes );
		return $this->quotes;
	}


	/**
	* check status of module
	*
	* @return boolean
	*/
	function check()
	{
		global $db;
		if (!isset($this->_check))
		{
			$check_query = $db->Execute(
				"SELECT configuration_value FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'MODULE_SHIPPING_SMARTSEND_STATUS'"
				);
			$this->_check = $check_query->RecordCount();
		}
		return $this->_check;
	}

	/**
	 * Let the install commence
	 */
	function install()
	{
		global $db, $sniffer;

		// Add extra product fields
		if (method_exists($sniffer, 'field_exists'))
		{
		if (! $sniffer->field_exists(TABLE_PRODUCTS, 'products_length')) $db->Execute("ALTER TABLE " . TABLE_PRODUCTS . " ADD products_length DECIMAL(6,2) DEFAULT '12' NOT NULL after products_weight");
		if (! $sniffer->field_exists(TABLE_PRODUCTS, 'products_width')) $db->Execute("ALTER TABLE " . TABLE_PRODUCTS . " ADD products_width DECIMAL(6,2) DEFAULT '12' NOT NULL after products_length");
		if (! $sniffer->field_exists(TABLE_PRODUCTS, 'products_height')) $db->Execute("ALTER TABLE " . TABLE_PRODUCTS . " ADD products_height DECIMAL(6,2) DEFAULT '12' NOT NULL after products_width");
		}

		// Set up Smart Send options
      $baseQry = 'INSERT INTO ' . TABLE_CONFIGURATION . ' ( configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added';

      foreach( $this->_keyConfigs as $key => $c )
      {
      	$qry = $baseQry;
      	if( count( $c ) == 6 ) $qry .= ', set_function';
      	$qry .= ") VALUES (
      	'$c[0]', '$key', '$c[1]', '$c[2]', '$c[3]', '$c[4]', NOW()";
      	if( count( $c ) == 6 ) $qry .= ", '" . addslashes( $c[5] ) . "'";
      	$qry .= ')';
			$db->Execute( $qry );
      }
	}


	function keys()
	{
		$keys = array_keys( $this->_keyConfigs );
		return array_keys( $this->_keyConfigs );
	}

	function remove()
	{
		global $db;
		$db->Execute( "DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_key IN ('" . implode( "', '", $this->keys() ) . "')" );
	}

	function _setConfigKeys()
	{
	// Let's set the config keys and their properties here, like grown-ups
		$this->_keyConfigs = array(
			'MODULE_SHIPPING_SMARTSEND_STATUS' => array(				// Key
				'Enable Smart Send Shipping',								// Title
				'True',															// Default value
				'Do you want to offer Smart Send shipping?',			// Description
				'6',																// Group ID
				'0',																// Sort order
				'zen_cfg_select_option(array(\'True\', \'False\'), ' // Set function
			),
        'MODULE_SHIPPING_SMARTSEND_USERNAME' => array(
				'Smart Send VIP Username',
				'',
				'Available from Smart Send',
				'6',
				'0'
			),
        'MODULE_SHIPPING_SMARTSEND_PASSWORD' => array(
				'Smart Send VIP Password',
				'',
				'Available from Smart Send, sign up <a href="https://www.smartsend.com.au/vipClientEnquiry.cfm" style="text-decoration: underline;" target="_blank">here</a>',
				'6',
				'0'
			),
        'MODULE_SHIPPING_SMARTSEND_FROM_TOWN' => array(
				'Origin Town',
				'',
				'Town goods are shipped from',
				'6',
				'0'
			),
        'MODULE_SHIPPING_SMARTSEND_FROM_POSTCODE' => array(
				'Origin Postcode',
				SHIPPING_ORIGIN_ZIP,
				'Postcode goods are shipped from',
				'6',
				'0'
			),
        'MODULE_SHIPPING_SMARTSEND_WIDTH' => array(
				'Default Width',
				'0',
				'Width used in shipping calculations if not specified for product.',
				'6',
				'0'
			),
        'MODULE_SHIPPING_SMARTSEND_LENGTH' => array(
				'Default Length',
				'0',
				'Length used in shipping calculations if not specified for product.',
				'6',
				'0'
			),
        'MODULE_SHIPPING_SMARTSEND_HEIGHT' => array(
				'Default Height',
				'0',
				'Height used in shipping calculations if not specified for product.',
				'6',
				'0'
			),
        'MODULE_SHIPPING_SMARTSEND_TYPE' => array(
				'Package Type',
				'Carton',
				'Package goods will be sent in',
				'6',
				'0'
			),
        'MODULE_SHIPPING_SMARTSEND_FEE_TYPE' => array(
				'Handling Fee Type',
				'None',
				'Available from Smart Send',
				'6',
				'0',
				'zen_cfg_select_option(array(\'None\', \'Flat\', \'Percentage\' ), '
			),
        'MODULE_SHIPPING_SMARTSEND_FEE_AMT' => array(
				'Handling Amount',
				'0',
				'Amount or Percentage',
				'6',
				'0'
			),
        'MODULE_SHIPPING_SMARTSEND_ASSURANCE' => array(
				'Transport Assurance',
				'No',
				'Insurance for packages',
				'6',
				'0',
				'zen_cfg_select_option(array(\'No\', \'Yes\', \'Optional\' ), '
			),
        'MODULE_SHIPPING_SMARTSEND_ASSURANCE_MIN' => array(
				'Min. Assurance Total',
				'0',
				'Minimum cart total to insure; if <i>Transport Assurance</i> is set to <b>optional</b>, user will only be offered assurance option if value is over this figure.',
				'6',
				'0'
			),
        'MODULE_SHIPPING_SMARTSEND_RECEIPTED' => array(
				'Receipted Delivery',
				'No',
				'Require signature on delivery; optional leaves it up to customer.',
				'6',
				'0',
				'zen_cfg_select_option(array(\'No\', \'Yes\', \'Optional\' ), '
			),
        'MODULE_SHIPPING_SMARTSEND_TAIL_LIFT' => array(
				'Tail Lift Pickup',
				'0',
				'Request tail-lift pickup for shipments containing an item with a weight over this (0 = disable)',
				'6',
				'0'
			),
        'MODULE_SHIPPING_SMARTSEND_TAIL_LIFT_DELIVERY' => array(
				'Tail Lift Delivery',
				'No',
				'Tail lift truck delivery for deliveries over 30kg. Optional gives customer the choice if an item is over 30kg.',
				'6',
				'0',
				'zen_cfg_select_option(array(\'No\', \'Yes\', \'Optional\' ), '
			),
        'MODULE_SHIPPING_SMARTSEND_SORT_ORDER' => array(
				'Sort order of display',
				'0',
				'Sort order of display. Lowest is displayed first.',
				'6',
				'0'
			)
		);

		$this->_keyConfigs['MODULE_SHIPPING_SMARTSEND_TYPE'][] = 'smartSendSelectHTML( array(\'' . implode( '\', \'', smartSendUtils::$ssPackageTypes ) . '\'), ';
	}
}