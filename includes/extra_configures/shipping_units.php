<?php

// Shipping units to be used/displayed throughout the store:
  define('TEXT_PRODUCT_WEIGHT_UNIT','kgs');
  define('TEXT_SHIPPING_WEIGHT','kgs');

  define('SYSTEM_WEIGHT_UNITS','kgs');
  define('SYSTEM_DIMENSION_UNITS','cm');
